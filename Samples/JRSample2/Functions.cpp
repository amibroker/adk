////////////////////////////////////////////////////
// Functions.cpp
// Sample functions implementation file for example AmiBroker plug-in
//
// Copyright (C)2001 Tomasz Janeczko, amibroker.com
// All rights reserved.
//
// Last modified: 2002-01-04 TJ
// 
// You may use this code in your own projects provided that:
//
// 1. You are registered user of AmiBroker
// 2. The software you write using it is for personal, noncommercial use only
//
// For commercial use you have to obtain a separate license from Amibroker.com
//
////////////////////////////////////////////////////

#include "Plugin.h"

/*
#pragma comment( lib, "jrs_32.lib" )
#define IMPORT extern "C" __declspec(dllimport)


IMPORT int WINAPI JMA( int iLength, double *pdSignal, double dSpeed, double dPhase, double *pdFilter );
IMPORT int WINAPI JMART( double dInput, double dSmooth, double dPhase, double *pdOutput, int iDestroy, int *piSeriesID );
IMPORT int WINAPI VEL( double *pdDataPtr, double *pdOutPtr, DWORD nDepth, DWORD nLength );
IMPORT int WINAPI RSX( DWORD nDataLength, double *pdSeries, double *pdRSX, double dLength );
IMPORT int WINAPI CFB( double *pdSeries, double *pdCFB, int iDataLength, int iSmooth, int iSpanSize );
IMPORT int WINAPI DMX( double *pdInHigh, double *pdInLow, double *pdInClose, double *pdOutBipolar, double *pdOutPlus, double *pdOutMinus, double dLength, int iSize );
*/
// Helper function

int SkipEmptyValues( int nSize, float *Src, float *Dst )
{
	int i;

	for( i = 0; i < nSize && IS_EMPTY( Src[ i ] ); i++ )
	{
		Dst[ i ] = EMPTY_VAL;
	}

	return i;
}



AmiVar VJurikJMA( int NumArgs, AmiVar *ArgsTable )
{
    int i, j, iRet, nSkipFirst;
    AmiVar result;

    result = gSite.AllocArrayResult();

	int nSize = gSite.GetArraySize();

	float *SrcArray = ArgsTable[ 0 ].array;
	double dSmooth =  ArgsTable[ 1 ].val;
	double dPhase =  ArgsTable[ 2 ].val;

	j = SkipEmptyValues( nSize, SrcArray, result.array );

	if( j < nSize )
	{
		// calculate only non-empty values

		///////////////////////////////////////
		// Jurik MA works on arrays of doubles
		// so we need to convert from floats
		// to double before calling JMA..........
		////////////////////////////////////////
		double *pdSignal = new double[ nSize - j ];
		double *pdFiltered = new double[ nSize - j ];

		for( i =  j; i < nSize; i++ )
		{
			pdSignal[ i - j ] = SrcArray[ i ];			
		}


		////////////////////////////////////////////
		// Now we call JMA from the DLL
		////////////////////////////////////////////
		//iRet = JMA(nSize-j, pdSignal, dSmooth, dPhase, pdFiltered);
		if(iRet) {
			for( i = j; i < nSize; i++ ) {
				result.array[ i ] = EMPTY_VAL;			
			}
		}
		else {
			nSkipFirst = 30;	
			// .... and convert the result array 
			// of doubles to floats after successfully calling JRS DLL
			for( i =  j; i < ( j + nSkipFirst ) && i < nSize; i++ ) {
				result.array[ i ] = EMPTY_VAL;			
			}

			for( ; i < nSize; i++ ) {
				result.array[ i ] = (float) pdFiltered[ i - j ];			
			}

		}
		////////////////////////////////////////////
		// Release temporary tables
		////////////////////////////////////////////
		delete[] pdSignal;
		delete[] pdFiltered;
	}
    return result;
}

/*
AmiVar VJurikJMAV( int NumArgs, AmiVar *ArgsTable )
{
    int i, j, iRet;
    AmiVar result;

    result = gSite.AllocArrayResult();

	int nSize = gSite.GetArraySize();

	float *SrcArray = ArgsTable[ 0 ].array;
	float *Smooth =  ArgsTable[ 1 ].array;
	float *Phase =  ArgsTable[ 2 ].array;

	j = SkipEmptyValues( nSize, SrcArray, result.array );

	if( j < nSize )
	{
		int iSeriesID = 0;
		double dOutput;
		
		for( i = j; i < nSize; i++ )
		{
			////////////////////////////////////////////
			// Now we call JMA from the DLL
			////////////////////////////////////////////
			//iRet = JMART(SrcArray[ i ], Smooth[ i ], Phase[ i ], &dOutput, 0, &iSeriesID);
			if(iRet) {
				result.array[i] = EMPTY_VAL;
			} 
			else {
				result.array[i] = i < j + 30 ? EMPTY_VAL : (float)dOutput;
			}
		}

		if( iSeriesID ) JMART( 0, 0, 0, 0, 1, &iSeriesID );
	}
	return result;
}

AmiVar VJurikCFB( int NumArgs, AmiVar *ArgsTable )
{
	int i, j, iRet;
	AmiVar result;

	result = gSite.AllocArrayResult();

	int nSize = gSite.GetArraySize();

	float *SrcArray = ArgsTable[ 0 ].array;
	int iSmooth = (int) ArgsTable[ 1 ].val;
	int iSpanSize = (int) ArgsTable[ 2 ].val;

	j = SkipEmptyValues(nSize, SrcArray, result.array);

	if( j < nSize )
	{
		// calculate only non-empty values

		///////////////////////////////////////
		// Jurik CFB works on arrays of doubles
		// so we need to convert from floats
		// to double before calling CFB..........
		////////////////////////////////////////
		double *pdSeries = new double[ nSize - j ];
		double *pdResult = new double[ nSize - j ];

		for( i =  j; i < nSize; i++ )
		{
			pdSeries[ i - j ] = SrcArray[ i ];			
		}


		////////////////////////////////////////////
		// Now we call CFB from the DLL
		////////////////////////////////////////////
		iRet = CFB(pdSeries, pdResult, nSize-j, iSmooth, iSpanSize);
		if(iRet) {
			for(i=j; i<nSize; i++) {
				result.array[i] = EMPTY_VAL;			
			}
		}
		else {
			// .... and convert the result array 
			// of doubles to floats after successfully calling JRS DLL
			int nSkipFirst = iSpanSize + 6;

			for( i =  j; i < ( j + nSkipFirst ) && i < nSize; i++ ){
				result.array[ i ] = EMPTY_VAL;			
			}

			for( ; i < nSize; i++ ) {
				result.array[ i ] = (float) pdResult[ i - j ];			
			}
		}

		////////////////////////////////////////////
		// Release temporary tables
		////////////////////////////////////////////
		delete[] pdSeries;
		delete[] pdResult;
	}
    return result;
}


AmiVar VJurikVEL( int NumArgs, AmiVar *ArgsTable )
{
    int i, j, iRet;
    AmiVar result;

    result = gSite.AllocArrayResult();

	int nSize = gSite.GetArraySize();

	float *SrcArray = ArgsTable[ 0 ].array;
	int iDepth = (int) ArgsTable[ 1 ].val;

	j = SkipEmptyValues( nSize, SrcArray, result.array );

	if( j < nSize )
	{
		// calculate only non-empty values

		///////////////////////////////////////
		// Jurik VEL works on arrays of doubles
		// so we need to convert from floats
		// to double before calling VEL..........
		////////////////////////////////////////
		double *pdSeries = new double[ nSize - j ];
		double *pdResult = new double[ nSize - j ];

		for( i =  j; i < nSize; i++ )	{
			pdSeries[ i - j ] = SrcArray[ i ];			
		}

		////////////////////////////////////////////
		// Now we call VEL from the DLL
		////////////////////////////////////////////
		iRet = VEL( pdSeries, pdResult, iDepth, nSize-j);
		if(iRet) {
			for(i=j; i<nSize; i++) {
				result.array[i] = EMPTY_VAL;			
			}
		}
		else {

			// .... and convert the result array 
			// of doubles to floats after successfully calling JRS DLL
			int nSkipFirst = iDepth > 30 ? iDepth : 30;

			for( i =  j; i < ( j + nSkipFirst ) && i < nSize; i++ ) {
				result.array[ i ] = EMPTY_VAL;			
			}

			for( ; i < nSize; i++ ) {
				result.array[ i ] = (float) pdResult[ i - j ];			
			}
		}

		////////////////////////////////////////////
		// Release temporary tables
		////////////////////////////////////////////
		delete[] pdSeries;
		delete[] pdResult;
	}
    return result;
}

AmiVar VJurikRSX( int NumArgs, AmiVar *ArgsTable )
{
    int i, j, iRet;
    AmiVar result;

    result = gSite.AllocArrayResult();

	int nSize = gSite.GetArraySize();

	float *SrcArray = ArgsTable[ 0 ].array;
	double dLength =  (double) ArgsTable[ 1 ].val;

	j = SkipEmptyValues( nSize, SrcArray, result.array );

	if( j < nSize )
	{
		// calculate only non-empty values

		///////////////////////////////////////
		// Jurik RSX works on arrays of doubles
		// so we need to convert from floats
		// to double before calling RSX..........
		////////////////////////////////////////
		double *pdSeries = new double[ nSize - j ];
		double *pdResult = new double[ nSize - j ];

		for(i= j; i < nSize; i++) {
			pdSeries[ i - j ] = SrcArray[ i ];			
		}

		////////////////////////////////////////////
		// Now we call RSX from the DLL
		////////////////////////////////////////////
		iRet = RSX(nSize-j, pdSeries, pdResult, dLength);
		if(iRet) {
			for(i=j; i<nSize; i++) {
				result.array[i] = EMPTY_VAL;			
			}
		}
		else {
			// .... and convert the result array 
			// of doubles to floats after successfully calling JRS DLL
			int nSkipFirst = 30;
		
			for( i =  j; i < ( j + nSkipFirst ) && i < nSize; i++ ) 	{
				result.array[ i ] = EMPTY_VAL;			
			}

			for( ; i < nSize; i++ )	{
				result.array[ i ] = (float) pdResult[ i - j ];			
			}
		}

		////////////////////////////////////////////
		// Release temporary tables
		////////////////////////////////////////////
		delete[] pdSeries;
		delete[] pdResult;
	}
    return result;
}

AmiVar VJurikDMX( int NumArgs, AmiVar *ArgsTable )
{
	int i, j, iRet;
	AmiVar result;

	result = gSite.AllocArrayResult();

	int nSize = gSite.GetArraySize();

	float *afHigh	= gSite.GetStockArray( 1 );
	float *afLow	= gSite.GetStockArray( 2 );
	float *afClose	= gSite.GetStockArray( 3 );

	double dLength =  ArgsTable[ 0 ].val;

	j = SkipEmptyValues( nSize, afClose, result.array );

	if( j < nSize )
	{
		// calculate only non-empty values

		///////////////////////////////////////
		// Jurik MA works on arrays of doubles
		// so we need to convert from floats
		// to double before calling JMA..........
		////////////////////////////////////////
		double *pdClose = new double[ nSize - j ];
		double *pdHigh = new double[ nSize - j ];
		double *pdLow = new double[ nSize - j ];
		double *pdOutput = new double[ nSize - j ];

		for( i =  j; i < nSize; i++ )
		{
			pdClose[ i - j ] = afClose[ i ];			
			pdHigh[ i - j ] = afHigh[ i ];			
			pdLow[ i - j ] = afLow[ i ];			
		}


		////////////////////////////////////////////
		// Now we call DMX from the DLL
		////////////////////////////////////////////
		iRet = DMX( pdHigh, pdLow, pdClose, pdOutput, NULL, NULL, dLength, nSize - j );
		if(iRet) {
			for(i=j; i<nSize; i++) {
				result.array[i] = EMPTY_VAL;			
			}
		}
		else {
			// .... and convert the result array 
			// of doubles to floats after successfully calling JRS DLL
			int nSkipFirst = 40;

			for( i =  j; i < ( j + nSkipFirst ) && i < nSize; i++ ) {
				result.array[ i ] = EMPTY_VAL;			
			}

			for( ; i < nSize; i++ )	{
				result.array[ i ] = (float) pdOutput[ i - j ];			
			}
		}

		////////////////////////////////////////////
		// Release temporary tables
		////////////////////////////////////////////
		delete[] pdOutput;
		delete[] pdHigh;
		delete[] pdLow;
		delete[] pdClose;
	}
    return result;
}

AmiVar VJurikDMXPlus( int NumArgs, AmiVar *ArgsTable )
{
	int i, j, iRet;
	AmiVar result;

	result = gSite.AllocArrayResult();

	int nSize = gSite.GetArraySize();

	float *afHigh	= gSite.GetStockArray( 1 );
	float *afLow	= gSite.GetStockArray( 2 );
	float *afClose	= gSite.GetStockArray( 3 );

	double dLength =  ArgsTable[ 0 ].val;

	j = SkipEmptyValues( nSize, afClose, result.array );

	if( j < nSize )
	{
		// calculate only non-empty values

		///////////////////////////////////////
		// Jurik MA works on arrays of doubles
		// so we need to convert from floats
		// to double before calling JMA..........
		////////////////////////////////////////
		double *pdClose = new double[ nSize - j ];
		double *pdHigh = new double[ nSize - j ];
		double *pdLow = new double[ nSize - j ];
		double *pdOutput = new double[ nSize - j ];

		for( i =  j; i < nSize; i++ )
		{
			pdClose[ i - j ] = afClose[ i ];			
			pdHigh[ i - j ] = afHigh[ i ];			
			pdLow[ i - j ] = afLow[ i ];			
		}


		////////////////////////////////////////////
		// Now we call DMX from the DLL
		////////////////////////////////////////////
		iRet = DMX( pdHigh, pdLow, pdClose, NULL, pdOutput, NULL, dLength, nSize - j );
		if(iRet) {
			for(i=j; i<nSize; i++) {
				result.array[i] = EMPTY_VAL;			
			}
		}
		else {
			// .... and convert the result array 
			// of doubles to floats after successfully calling JRS DLL
			int nSkipFirst = 40;

			for( i =  j; i < ( j + nSkipFirst ) && i < nSize; i++ ) {
				result.array[ i ] = EMPTY_VAL;			
			}

			for( ; i < nSize; i++ ) {
				result.array[ i ] = (float) pdOutput[ i - j ];			
			}
		}

		////////////////////////////////////////////
		// Release temporary tables
		////////////////////////////////////////////
		delete[] pdOutput;
		delete[] pdHigh;
		delete[] pdLow;
		delete[] pdClose;
	}
    return result;
}

AmiVar VJurikDMXMinus( int NumArgs, AmiVar *ArgsTable )
{
	int i, j, iRet;
	AmiVar result;

	result = gSite.AllocArrayResult();
	int nSize = gSite.GetArraySize();

	float *afHigh	= gSite.GetStockArray( 1 );
	float *afLow	= gSite.GetStockArray( 2 );
	float *afClose	= gSite.GetStockArray( 3 );

	double dLength =  ArgsTable[ 0 ].val;

	j = SkipEmptyValues( nSize, afClose, result.array );

	if( j < nSize )
	{
		// calculate only non-empty values

		///////////////////////////////////////
		// Jurik MA works on arrays of doubles
		// so we need to convert from floats
		// to double before calling JMA..........
		////////////////////////////////////////
		double *pdClose = new double[ nSize - j ];
		double *pdHigh = new double[ nSize - j ];
		double *pdLow = new double[ nSize - j ];
		double *pdOutput = new double[ nSize - j ];

		for( i =  j; i < nSize; i++ )
		{
			pdClose[ i - j ] = afClose[ i ];			
			pdHigh[ i - j ] = afHigh[ i ];			
			pdLow[ i - j ] = afLow[ i ];			
		}


		////////////////////////////////////////////
		// Now we call DMX from the DLL
		////////////////////////////////////////////
		iRet = DMX( pdHigh, pdLow, pdClose, NULL, NULL, pdOutput, dLength, nSize - j );
		if(iRet) {
			for(i=j; i<nSize; i++) {
				result.array[i] = EMPTY_VAL;			
			}
		}
		else {
			// .... and convert the result array 
			// of doubles to floats after successfully calling JRS DLL
			int nSkipFirst = 40;

			for( i =  j; i < ( j + nSkipFirst ) && i < nSize; i++ ) {
				result.array[ i ] = EMPTY_VAL;			
			}

			for( ; i < nSize; i++ ) {
				result.array[ i ] = (float) pdOutput[ i - j ];			
			}
		}
		////////////////////////////////////////////
		// Release temporary tables
		////////////////////////////////////////////
		delete[] pdOutput;
		delete[] pdHigh;
		delete[] pdLow;
		delete[] pdClose;
	}
    return result;
}

*/
float dmx_def[] = { 14 };
float jma_def[] = { 8, 50 };
float rsx_def[] = { 14 };
float vel_def[] = { 10 };
float cfb_def[] = { 8, 24 };

/////////////////////////////////////////////
// Function table now follows
//
// You have to specify each function that should be
// visible for AmiBroker.
// Each entry of the table must contain:
// "Function name", { FunctionPtr, <no. of array args>, <no. of string args>, <no. of float args>, <no. of default args>, <pointer to default values table float *>

FunctionTag gFunctionTable[] = {
								"JurikJMA",        { VJurikJMA, 1, 0, 0, 2, jma_def }, 
//								"JurikJMAV",       { VJurikJMAV, 3, 0, 0, 0, NULL }, 
//								"JurikCFB",        { VJurikCFB, 1, 0, 0, 2, cfb_def }, 
//								"JurikVEL",        { VJurikVEL, 1, 0, 0, 1, vel_def }, 
//								"JurikRSX",        { VJurikRSX, 1, 0, 0, 1, rsx_def }, 
//								"JurikDMX",        { VJurikDMX, 0, 0, 0, 1, dmx_def }, 
//								"JurikDMXPlus",    { VJurikDMXPlus, 0, 0, 0, 1, dmx_def }, 
//								"JurikDMXMinus",   { VJurikDMXMinus, 0, 0, 0, 1, dmx_def }, 
};

int gFunctionTableSize = sizeof( gFunctionTable )/sizeof( FunctionTag );
